﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Object = UnityEngine.Object;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Lunar.UI {
    public class UIRaycaster : GraphicRaycaster {

        [NonSerialized] private static readonly List<Graphic> s_SortedGraphics = new List<Graphic>();
        [NonSerialized] private readonly List<Graphic> m_RaycastResults = new List<Graphic>();
        private Canvas canvas;

        public float DistanceToUiObject = 3f;

        protected override void Start() {
            canvas = GetComponent<Canvas>();
        }

        public override void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList) {
            if (canvas == null) return;

            Vector2 vector2 = !(eventCamera == null) ?
                              (Vector2) eventCamera.ScreenToViewportPoint(eventData.position) :
                              new Vector2(eventData.position.x / Screen.width, eventData.position.y / Screen.height);

            if (vector2.x < 0.0 || vector2.x > 1.0 || (vector2.y < 0.0 || vector2.y > 1.0)) return;

            float num1 = float.MaxValue;

            if (canvas.renderMode != RenderMode.ScreenSpaceOverlay && blockingObjects != BlockingObjects.None) {
                Ray ray = eventCamera.ScreenPointToRay(eventData.position);
                float distance = eventCamera.farClipPlane - eventCamera.nearClipPlane;

                if (blockingObjects == BlockingObjects.ThreeD || blockingObjects == BlockingObjects.All) {
                    RaycastHit[] raycastHitArray = UnityEngine.Physics.RaycastAll(ray, distance, m_BlockingMask);
                    if (raycastHitArray.Length > 0 && raycastHitArray[0].distance < (double) num1)
                        num1 = raycastHitArray[0].distance;
                }

                if (blockingObjects == BlockingObjects.TwoD || blockingObjects == BlockingObjects.All) {
                    RaycastHit2D[] rayIntersectionAll = Physics2D.GetRayIntersectionAll(ray, distance, m_BlockingMask);
                    if (rayIntersectionAll.Length > 0 && rayIntersectionAll[0].fraction * (double) distance < num1)
                        num1 = rayIntersectionAll[0].fraction * distance;
                }
            }

            m_RaycastResults.Clear();
            Raycast(canvas, eventCamera, eventData.position, m_RaycastResults);

            foreach (Graphic rGraphic in m_RaycastResults) {
                GameObject go = rGraphic.gameObject;

                bool flag = true;

                if (ignoreReversedGraphics)
                    flag = !(eventCamera == null) ? Vector3.Dot(eventCamera.transform.rotation * Vector3.forward,
                        go.transform.rotation * Vector3.forward) > 0.0 :
                        Vector3.Dot(Vector3.forward, go.transform.rotation * Vector3.forward) > 0.0;

                if (flag) {
                    float num2 = (Object) eventCamera == (Object) null || canvas.renderMode == RenderMode.ScreenSpaceOverlay
                        ? 0.0f : Vector3.Distance(eventCamera.transform.position, canvas.transform.position);

                    if (EventSystem.current.currentSelectedGameObject == go) {
                        if (num2 > (double) DistanceToUiObject) {
                            EventSystem.current.SetSelectedGameObject(null);
                        }
                    }

                    if (num2 < (double) DistanceToUiObject) {

                        var raycastResult = new RaycastResult {
                            gameObject = go,
                            module = this,
                            distance = num2,
                            index = resultAppendList.Count,
                            depth = rGraphic.depth
                        };
                        resultAppendList.Add(raycastResult);
                    }
                }
            }
        }


        private static void Raycast(Canvas canvas, Camera eventCamera, Vector2 pointerPosition, List<Graphic> results) {
            IList<Graphic> graphicsForCanvas = GraphicRegistry.GetGraphicsForCanvas(canvas);
            s_SortedGraphics.Clear();
            foreach (Graphic graphic in graphicsForCanvas) {
                if (graphic.depth != -1 &&
                    RectTransformUtility.RectangleContainsScreenPoint(graphic.rectTransform, pointerPosition,
                        eventCamera) && graphic.Raycast(pointerPosition, eventCamera))
                    s_SortedGraphics.Add(graphic);
            }
            s_SortedGraphics.Sort((g1, g2) => g2.depth.CompareTo(g1.depth));
            results.AddRange(s_SortedGraphics);
        }
    }
}