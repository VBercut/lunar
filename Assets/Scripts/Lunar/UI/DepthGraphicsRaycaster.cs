﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.Lunar.UI {
    public class DepthGraphicsRaycaster : BaseRaycaster, ISerializationCallbackReceiver {
    [NonSerialized]
    protected static readonly List<Graphic> s_SortedGraphics = new List<Graphic>();
    [SerializeField]
    protected int m_Priority = -1;
    public bool ignoreReversedGraphics = true;
    [SerializeField]
    //protected LayerMask m_BlockingMask = (LayerMask) -1;
    protected LayerMask m_BlockingMask = -1;
    [NonSerialized]
    protected List<GameObject> m_RaycastResults = new List<GameObject>();
    protected const int kNoEventMaskSet = -1;
    public DepthGraphicsRaycaster.BlockingObjects blockingObjects;
    protected Canvas m_Canvas;

    public float RayLenght = 1.5f;

    public override int priority
    {
      get
      {
        return this.m_Priority;
      }
    }

    protected Canvas canvas
    {
      get
      {
        if ((UnityEngine.Object) this.m_Canvas != (UnityEngine.Object) null)
          return this.m_Canvas;
        this.m_Canvas = this.GetComponent<Canvas>();
        return this.m_Canvas;
      }
    }

    public override Camera eventCamera
    {
      get
      {
          if (this.canvas.renderMode == RenderMode.ScreenSpaceOverlay || this.canvas.renderMode == RenderMode.ScreenSpaceOverlay && (UnityEngine.Object)this.canvas.worldCamera == (UnityEngine.Object)null)
          return (Camera) null;
        if ((UnityEngine.Object) this.canvas.worldCamera != (UnityEngine.Object) null)
          return this.canvas.worldCamera;
        else
          return Camera.main;
      }
    }

    public int defaultPriority
    {
      get
      {
        return 3;
      }
    }

    static DepthGraphicsRaycaster()
    {
    }

    protected DepthGraphicsRaycaster()
    {
    }

    void ISerializationCallbackReceiver.OnBeforeSerialize()
    {
    }

    void ISerializationCallbackReceiver.OnAfterDeserialize()
    {
      if (this.m_Priority > 0)
        return;
      this.m_Priority = this.defaultPriority;
    }

    public override void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList)
    {
      if ((UnityEngine.Object) this.canvas == (UnityEngine.Object) null)
        return;
      Vector2 vector2 = !((UnityEngine.Object) this.eventCamera == (UnityEngine.Object) null) ? (Vector2) this.eventCamera.ScreenToViewportPoint((Vector3) eventData.position) : new Vector2(eventData.position.x / (float) Screen.width, eventData.position.y / (float) Screen.height);
      if ((double) vector2.x < 0.0 || (double) vector2.x > 1.0 || ((double) vector2.y < 0.0 || (double) vector2.y > 1.0))
        return;
      float num1 = float.MaxValue;
      if (this.canvas.renderMode != RenderMode.ScreenSpaceOverlay && this.blockingObjects != BlockingObjects.None)
      {
        Ray ray = this.eventCamera.ScreenPointToRay((Vector3) eventData.position);
        float distance = this.eventCamera.farClipPlane - this.eventCamera.nearClipPlane;
        if (this.blockingObjects == BlockingObjects.ThreeD || this.blockingObjects == BlockingObjects.All)
        {
          RaycastHit[] raycastHitArray = UnityEngine.Physics.RaycastAll(ray, distance, (int) this.m_BlockingMask);
          if (raycastHitArray.Length > 0 && (double) raycastHitArray[0].distance < (double) num1)
            num1 = raycastHitArray[0].distance;
        }
        if (this.blockingObjects == BlockingObjects.TwoD || this.blockingObjects == BlockingObjects.All)
        {
          RaycastHit2D[] rayIntersectionAll = Physics2D.GetRayIntersectionAll(ray, distance, (int) this.m_BlockingMask);
          if (rayIntersectionAll.Length > 0 && (double) rayIntersectionAll[0].fraction * (double) distance < (double) num1)
            num1 = rayIntersectionAll[0].fraction * distance;
        }
      }
      this.m_RaycastResults.Clear();
      DepthGraphicsRaycaster.Raycast(this.canvas, this.eventCamera, eventData.position, this.m_RaycastResults);
      for (int index = 0; index < this.m_RaycastResults.Count; ++index)
      {
        GameObject gameObject = this.m_RaycastResults[index];
        bool flag = true;
        if (this.ignoreReversedGraphics)
          flag = !((UnityEngine.Object) this.eventCamera == (UnityEngine.Object) null) ? (double) Vector3.Dot(this.eventCamera.transform.rotation * Vector3.forward, gameObject.transform.rotation * Vector3.forward) > 0.0 : (double) Vector3.Dot(Vector3.forward, gameObject.transform.rotation * Vector3.forward) > 0.0;
        if (flag)
        {
            float num2 = this.canvas.renderMode != RenderMode.ScreenSpaceOverlay ? Vector3.Distance(this.eventCamera.transform.position, this.canvas.transform.position) : 0.0f;
          if ((double) num2 < (double) num1)
          {
            var raycastResult = new RaycastResult()
            {
              gameObject = gameObject,
              module = (BaseRaycaster) this,
              distance = num2,
              index = (float) resultAppendList.Count
            };
            resultAppendList.Add(raycastResult);
          }
        }
      }
    }

    protected static void Raycast(Canvas canvas, Camera eventCamera, Vector2 pointerPosition, List<GameObject> results)
    {
      IList<Graphic> graphicsForCanvas = GraphicRegistry.GetGraphicsForCanvas(canvas);
      DepthGraphicsRaycaster.s_SortedGraphics.Clear();
      for (int index = 0; index < graphicsForCanvas.Count; ++index)
      {
        Graphic graphic = graphicsForCanvas[index];
        if (graphic.depth != -1 && RectTransformUtility.RectangleContainsScreenPoint(graphic.rectTransform, pointerPosition, eventCamera) && graphic.Raycast(pointerPosition, eventCamera))
            DepthGraphicsRaycaster.s_SortedGraphics.Add(graphic);
      }
      DepthGraphicsRaycaster.s_SortedGraphics.Sort((Comparison<Graphic>)((g1, g2) => g2.depth.CompareTo(g1.depth)));
      for (int index = 0; index < DepthGraphicsRaycaster.s_SortedGraphics.Count; ++index)
          results.Add(DepthGraphicsRaycaster.s_SortedGraphics[index].gameObject);
    }

    public enum BlockingObjects
    {
      None,
      TwoD,
      ThreeD,
      All,
    }
  }
}
