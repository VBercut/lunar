﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.Lunar.UI {
    internal class CustomDepthGraphicsRaycaster : DepthGraphicsRaycaster {
        public override void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList) {
            if (canvas == null) return;

            resultAppendList.Clear();

            Vector2 vector2 = !(eventCamera == null)
                ? (Vector2) eventCamera.ScreenToViewportPoint(eventData.position)
                : new Vector2(eventData.position.x / Screen.width, eventData.position.y / Screen.height);
            if (vector2.x < 0.0 || vector2.x > 1.0 || (vector2.y < 0.0 || vector2.y > 1.0)) return;

            float num1 = RayLenght;


/*            if (canvas.renderMode != RenderMode.Overlay && blockingObjects != BlockingObjects.None) {
                Ray ray = eventCamera.ScreenPointToRay(eventData.position);

                //Ray ray = Camera.main.ScreenPointToRay(new Vector2((float)Screen.width / 2, (float)Screen.height / 2));

                float distance = eventCamera.farClipPlane - eventCamera.nearClipPlane;
                //float distance = RayLenght;

                if (blockingObjects == BlockingObjects.ThreeD || blockingObjects == BlockingObjects.All) {
                    RaycastHit[] raycastHitArray = UnityEngine.Physics.RaycastAll(ray, distance, m_BlockingMask);
                    if (raycastHitArray.Length > 0 && raycastHitArray[0].distance < (double) num1)
                        num1 = raycastHitArray[0].distance;
                }
                if (blockingObjects == BlockingObjects.TwoD || blockingObjects == BlockingObjects.All) {
                    RaycastHit2D[] rayIntersectionAll = Physics2D.GetRayIntersectionAll(ray, distance, m_BlockingMask);
                    if (rayIntersectionAll.Length > 0 && rayIntersectionAll[0].fraction * (double) distance < num1)
                        num1 = rayIntersectionAll[0].fraction * distance;
                }
            }*/

            m_RaycastResults.Clear();
            Raycast(canvas, eventCamera, eventData.position, m_RaycastResults);

            foreach (GameObject go in m_RaycastResults) {
                bool flag = true;
                if (ignoreReversedGraphics)
                    flag = !(eventCamera == null)
                        ? Vector3.Dot(eventCamera.transform.rotation * Vector3.forward,
                            go.transform.rotation * Vector3.forward) > 0.0
                        : Vector3.Dot(Vector3.forward, go.transform.rotation * Vector3.forward) > 0.0;
                if (flag) {
                    float num2 = canvas.renderMode != RenderMode.ScreenSpaceOverlay
                        ? Vector3.Distance(eventCamera.transform.position, canvas.transform.position)
                        : 0.0f;

                    var selectable = go.GetComponent<Selectable>();

                    if (selectable != null) {

                        selectable.OnDeselect(null);

                        //selectable.interactable = num2 < (double) num1;
                    }

                    if (num2 < (double) num1) {

                        if (selectable != null) {
                            selectable.OnSelect(null);
                        }

                        var raycastResult = new RaycastResult {
                            gameObject = go,
                            module = this,
                            distance = num2,
                            index = resultAppendList.Count
                        };
                        resultAppendList.Add(raycastResult);
                    }
                }
            }
        }


    }
}