﻿using UnityEngine;

namespace Assets.Scripts.Lunar.Physics {
    public class ObjectPuller : MonoBehaviour {
        public JointLimiter JointLimiter;
        public float MovePower = 3f;
        public float PullPower = 3f;
        public float RayLenght = 3f;

        private DeltaPositionObject _deltaPositionObject;

        // Use this for initialization
        private void Start() {
            _deltaPositionObject = GetComponent<DeltaPositionObject>();
        }

        // Update is called once per frame
        private void Update() {
            if (Input.GetButton("Fire1")) {
                Ray ray = Camera.main.ScreenPointToRay(new Vector2((float) Screen.width / 2, (float) Screen.height / 2));
                RaycastHit hit;

                if (UnityEngine.Physics.Raycast(ray, out hit, RayLenght)) {

                    if (!hit.collider.CompareTag("Pullable")) return;

                    GameObject hitGameObject = hit.collider.gameObject;
                    Transform parent = hitGameObject.transform.parent;

                    if (parent == null) return;

                    JointLimiter = parent.GetComponent<JointLimiter>();
                    if (JointLimiter == null) return;

                    JointLimiter.IsKinematic = true;

                    Vector3 resultPower = ray.direction * PullPower * Input.GetAxis("Mouse Y") / parent.rigidbody.mass;
                    resultPower *= -1;

                    if (_deltaPositionObject != null) {
                        if (!Mathf.Approximately(_deltaPositionObject.DeltaPosition.x, 0)) {
                            resultPower = ray.direction * MovePower * PullPower * _deltaPositionObject.DeltaPosition.x /
                                          parent.rigidbody.mass;
                        }
                    }

                    if (Mathf.Approximately(resultPower.y, 0)) return;

                    JointLimiter.Move(resultPower, hit.point);
                }
            }

            if (JointLimiter == null) return;

            if (Input.GetButtonUp("Fire1")) {
                JointLimiter.IsKinematic = false;
                JointLimiter = null;
            }
        }
    }
}