﻿using UnityEngine;

namespace Assets.Scripts.Lunar.Physics {
    public class JointLimiter : MonoBehaviour {

        public bool LimitX;
        public bool LimitY;
        public bool LimitZ;

        public Vector2 Rotation;

        public bool IsKinematic;

        private Vector3 _previousAngles;

        // Use this for initialization
        private void Start() {
            _previousAngles = transform.localEulerAngles;
        }

        // Update is called once per frame
        private void Update() {}

        private void FixedUpdate() {
            rigidbody.isKinematic = IsKinematic;

            Vector3 angles = transform.localEulerAngles;

            angles.x = LimitX ? LimitValue(angles.x, _previousAngles.x) : 0;
            angles.y = LimitY ? LimitValue(angles.y, _previousAngles.y) : 0;
            angles.z = LimitZ ? LimitValue(angles.z, _previousAngles.z) : 0;

            transform.localEulerAngles = angles;

            _previousAngles = new Vector3(angles.x, angles.y, angles.z);
        }

        private float LimitValue(float value, float previousValue) {

            float newValue = Mathf.Clamp(value, Rotation.x, Rotation.y);

            if (value > previousValue && value > Rotation.y && previousValue < Rotation.y) {
                newValue = Rotation.x;
            }

            if (Mathf.Approximately(newValue, Rotation.x) || Mathf.Approximately(newValue, Rotation.y)) {
                rigidbody.isKinematic = true;
            }

            return newValue;
        }

        public void Move(Vector3 resultPower, Vector3 point) {

            // Debug.Log("Side: " + (transform.forward - point));

            Vector3 angles = transform.localEulerAngles;

            if (LimitX) {
                angles.x += resultPower.y;
            }

            if (LimitY) {
                angles.y += resultPower.y;
            }

            if (LimitZ) {
                angles.z += resultPower.y;
            }

            transform.localEulerAngles = angles;
        }
    }
}