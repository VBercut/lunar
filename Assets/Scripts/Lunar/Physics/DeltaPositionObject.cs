﻿using UnityEngine;

namespace Assets.Scripts.Lunar.Physics {
    public class DeltaPositionObject : MonoBehaviour {

        private Vector3 _lastPosition;
        public Vector3 DeltaPosition;

        // Use this for initialization
        void Start () {
            _lastPosition = transform.position;
        }
	
        // Update is called once per frame
        void Update () {
            DeltaPosition = _lastPosition - transform.position;
            _lastPosition = transform.position;
        }
    }
}
