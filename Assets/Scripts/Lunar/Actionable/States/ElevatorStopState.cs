﻿using Assets.Scripts.ActionableLibrary.Objects.States;
using Assets.Scripts.Lunar.Control;

namespace Assets.Scripts.Lunar.Actionable.States {
    public class ElevatorStopState : ActionableObjectState {
        public override void Enter() {
            Entity.AddListener<string>(RemoteControl.Actions.ACTION, OnAction);

            Entity.SetField("moving", false);

            if (Entity.HasField("up")) {}
            else {
                Entity.SetField("up", true);
                Entity.SetField("start", transform.position.y);
            }
        }

        private void OnAction(string type) {

            var up = (bool) Entity.GetValue("up");

            if (up && type == RemoteControl.Actions.DOWN) {
                Entity.ChangeState("move");  
            }

            if (!up && type == RemoteControl.Actions.UP) {
                Entity.ChangeState("move");
            }
        }

        public override void Execute() {
            base.Execute();
        }

        public override void Exit() {
            Entity.RemoveListener<string>(RemoteControl.Actions.ACTION, OnAction);
        }
    }
}