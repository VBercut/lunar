﻿using Assets.Scripts.ActionableLibrary.Objects.States;
using UnityEngine;

namespace Assets.Scripts.Lunar.Actionable.States {
    public class ElevatorMoveState : ActionableObjectState {
        private float len = 13.8586f;

        public override void Enter() {
            Entity.SetField("length", len);

            var up = (bool) Entity.GetValue("up");
            var start = (float) Entity.GetValue("start");

            Entity.SetField("moving", true);

            float aim = start;

            if (up) {
                Entity.SetField("finish", start - (float) Entity.GetValue("length"));

                aim = (float) Entity.GetValue("finish");
            }

            iTween.MoveTo(gameObject,
                iTween.Hash("y", aim, "easetype", iTween.EaseType.easeInOutQuint, "oncomplete", "onFinish", "speed", 0.9f));
        }

        private void onFinish() {
            Debug.Log("Done");

            var up = (bool) Entity.GetValue("up");
            Entity.SetField("up", !up);

            Entity.ChangeState("stop");
        }

        public override void Execute() {}

        public override void Exit() {
            base.Exit();
        }
    }
}