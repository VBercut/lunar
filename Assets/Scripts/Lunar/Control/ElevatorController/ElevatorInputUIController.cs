﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Lunar.Control.ElevatorController {
    public class ElevatorInputUIController : ActionableReader {

        private Canvas _canvas;

        private void Start() {
            _canvas = GetComponent<Canvas>();
        }

        public void Update() {
            if(!_canvas) return;

            if(Objects.Count == 0) return;
            var obj = Objects.First();

            if(!obj.HasField("moving")) return;

            var moving = (bool) obj.GetValue("moving");

            _canvas.enabled = !moving;
        }

    }
}
