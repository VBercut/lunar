﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.ActionableLibrary.Objects.Items;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Lunar.Control.ElevatorController {
    internal class ElevatorMovingUIController : ActionableReader {
        private Canvas _canvas;
        private List<Image> images;

        private int _currentImage = 0;
        private float _currentTime = 0;

        public float ChangeArrowDelay = 2.5f;
        public float ChangeArrowSpeed = 0.5f;

        public Color NormalColor = Color.black;
        public Color MovingColor = Color.white;

        private void Start() {
            _canvas = GetComponent<Canvas>();
            images = new List<Image>();

            int childrenCount = transform.childCount;

            for (int i = 0; i < childrenCount; i++) {
                var image = transform.GetChild(i).GetComponent<Image>();
                if (image == null) continue;

                images.Add(image);
            }
        }

        public void Update() {
            if (!_canvas) return;

            if (Objects.Count == 0) return;
            ActionableInteractiveObject obj = Objects.First();

            if (!obj.HasField("moving")) return;

            var moving = (bool) obj.GetValue("moving");
            var up = (bool) obj.GetValue("up");

            _canvas.enabled = moving;

            if (!moving) {
                _currentTime = 0;
                _currentImage = up ? 0 : images.Count - 1;

                return;
            }

            if (_currentTime > ChangeArrowDelay) {

                _currentTime = 0;

                if (up) {
                    _currentImage++;
                } else {
                    _currentImage--;
                }

                if (_currentImage >= images.Count) {
                    _currentImage = 0;
                }

                if (_currentImage < 0) {
                    _currentImage = images.Count - 1;
                }
            }

            _currentTime += ChangeArrowSpeed * Time.deltaTime;

            Image movingImage = images[_currentImage];

            foreach (Image image in images) {
                Vector3 rotation = image.transform.localEulerAngles;

                rotation = up ? new Vector3(rotation.x, rotation.y, 0) : new Vector3(rotation.x, rotation.y, 180);

                image.transform.localEulerAngles = rotation;

                image.color = image == movingImage ? MovingColor : NormalColor;
            }
        }
    }
}