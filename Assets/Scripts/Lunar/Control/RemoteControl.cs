﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Assets.ActionableLibrary.Objects.Core;
using Assets.ActionableLibrary.Objects.Items;
using Assets.Scripts.ActionableLibrary.Objects.Core;
using Assets.Scripts.ActionableLibrary.Objects.Items;

namespace Assets.Scripts.Lunar.Control {
    public class RemoteControl : ActionableInteractiveObject {

        public class Actions {
            public const string UP = "up";
            public const string DOWN = "down";
            public const string ACTION = "action";
        }


        public List<MainInteractiveObject> Objects;

        protected override void Start() {
            
        }


        public void OnAction(String type) {

            UnityEngine.Debug.Log("Hello remote control: " + type);

            foreach (var obj in Objects) {
                obj.Send(Actions.ACTION, type);
            }
        }

    }
}
