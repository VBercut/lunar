﻿using UnityEngine;

namespace Assets.Scripts.Lunar.Player {
    public class StepCameraEffect : MonoBehaviour {

        public CharacterController Controller;
        public Animation Anim;

        public float StepSize = 0.5f;

        private bool _side;


        // Use this for initialization
        void Start () {
	
        }
	
        // Update is called once per frame
        void Update () {

            if(!Controller.isGrounded) return;
            if (Input.GetAxis("Horizontal") < StepSize && Input.GetAxis("Vertical") < StepSize) return;
            // if(Mathf.Approximately(Input.GetAxis("Horizontal"), 0) && Mathf.Approximately(Input.GetAxis("Vertical"), 0)) return;

            if(Anim.isPlaying) return;
            string currentAnimation = _side ? "walkRight" : "walkLeft";

            Anim.Play(currentAnimation);
            _side = !_side;
	
        }
    }
}
