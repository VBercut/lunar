﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Lunar.Player {
    public class PlatformAttacher : MonoBehaviour {

        private readonly HashSet<GameObject> _enteredColliders = new HashSet<GameObject>();
        private CharacterController _controller;
        private bool _lastGround;
        private GameObject _lastObj;

        private string _lastTag;

        private const string PlatformTag = "Platform";

        private void Start() {
            _controller = GetComponent<CharacterController>();
        }

        private void OnControllerColliderHit(ControllerColliderHit hit) {
            _enteredColliders.Add(hit.gameObject);
        }

        private void FixedUpdate() {

            GameObject parent = _enteredColliders.FirstOrDefault();

            string currentTag = parent ? parent.tag : "";
            string parentTag = "";

            try {
                parentTag = parent.transform.parent.tag;
            } catch (Exception) {
            }

            if (currentTag != _lastTag) {
                transform.parent = (currentTag == PlatformTag || parentTag == PlatformTag) ? parent.transform : null;
            }

            _lastTag = currentTag;
            _enteredColliders.Clear();

            _lastTag = currentTag;
        }
    }
}