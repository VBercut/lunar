﻿using UnityEngine;

namespace Assets.Scripts.Lunar.Player {
    public class LockCursor : MonoBehaviour {
        public Texture2D Cursor;
        // Update is called once per frame
        private void Update() {
            Screen.lockCursor = true;
        }

        private void OnGUI() {
            if (Cursor) {

                Vector2 offset = new Vector2(Cursor.width / 4, Cursor.height / 4);
                Vector2 size = new Vector2(Cursor.width / 2, Cursor.height / 2);

                GUI.DrawTexture(new Rect(Screen.width / 2 - offset.x, Screen.height / 2 - offset.y, size.x, size.y), Cursor);
            }
                
        }
    }
}