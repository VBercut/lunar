using Assets.ActionableLibrary;
using Assets.Scripts.Objects.Core;
using UnityEngine;

namespace Assets.Scripts {
    public class InteractivePlayer : MonoBehaviour {
        // Use this for initialization

        private readonly Color c1 = Color.red;
        private readonly Color c2 = Color.red;
        private GameObject currentItem;

        public float distance = 15f;
        private float laserWidth = 0.01f;
        private int lengthOfLineRenderer = 2;
        private Transform lookAtTransform;

        private bool showLaser = true;

        private void Start() {
            var lineRenderer = gameObject.AddComponent<LineRenderer>();
            lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
            lineRenderer.SetColors(c1, c2);
            lineRenderer.SetWidth(laserWidth, laserWidth);
            lineRenderer.SetVertexCount(lengthOfLineRenderer);

            Screen.showCursor = false; 

            lookAtTransform = GameObject.FindWithTag("MainCamera").GetComponent<Transform>();
        }

        // Update is called once per frame
        private void Update() {
            // ��������������� ������� � ������� ����������� � �������� �� ������ y, �����
            // ������� �� �������, � �� ������ ������� ����

            Vector3 lookFwd = lookAtTransform.TransformDirection(Vector3.forward);

            Vector3 fwd = transform.TransformDirection(Vector3.forward);

            fwd.y = lookFwd.y;

            Vector3 position = transform.position;

            position.y = lookAtTransform.position.y * 4 / 5;

            RaycastHit hit;

            Physics.Raycast(position, fwd, out hit, distance);

            GameObject newItem = hit.collider != null ? hit.collider.gameObject : null;

            if (currentItem != newItem) {
                // ������� ������� ��������, ��� �� ������ �� � ������
                Messenger.Broadcast(ItemEvents.ACTION, PlayerActions.LostItemFocus);

                if (newItem != null) {
                    var interactiveObject = newItem.GetComponent<InteractiveObject>();
                    if (interactiveObject != null) {
                        Messenger.AddListener<string>(ItemEvents.ACTION, interactiveObject.OnAction);
                        Messenger.Broadcast(ItemEvents.ACTION, PlayerActions.GetItemFocus);
                    }
                }
                currentItem = newItem;
            }

            if (Input.GetMouseButtonUp(0)) {
                Messenger.Broadcast(ItemEvents.ACTION, PlayerActions.Action);
                Debug.Log("Mouse 0!~");
            }

            if (Input.GetAxis("Mouse ScrollWheel") != 0) {
                Messenger.Broadcast(ItemEvents.ACTION, PlayerActions.RollWheel);
            }

            if (Input.GetKeyDown(KeyCode.L)) {
                showLaser = !showLaser;
            }

            var lineRenderer = GetComponent<LineRenderer>();

            if (showLaser) {
                lineRenderer.SetWidth(laserWidth, laserWidth);
                lineRenderer.SetPosition(0, position);

                Vector3 newPos = fwd;

                newPos.x *= distance;
                newPos.y *= distance;
                newPos.z *= distance;

                newPos = position + newPos;

                lineRenderer.SetPosition(1, newPos);
            }
            else {
                lineRenderer.SetWidth(0, 0);
            }
        }
    }
}