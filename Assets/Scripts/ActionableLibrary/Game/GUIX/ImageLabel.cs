﻿using System;
using Assets.ActionableLibrary.Objects.Core;
using Assets.Scripts.ActionableLibrary.Objects.Core;
using Assets.Scripts.Objects.Core;
using UnityEngine;

namespace Assets.Scripts.Game.GUIX {
    public class ImageLabel : MainInteractiveObject {
        public float Duration = 1;
        public Texture2D Pic;
        public float ScaleValue = 1.0f;
        public bool Show = true;
        private float _oldScale;


        private void Start() {
            _oldScale = ScaleValue;
            ResizePic();
        }

        private void Update() {
            if (Math.Abs(ScaleValue - _oldScale) > 0.001f) {
                ResizePic();
            }
        }

        private void ResizePic() {
            var w = (int) (Pic.width * ScaleValue);
            var h = (int) (Pic.height * ScaleValue);

            //Pic.Resize(w, h);
        }

        private void OnGUI() {
            if (Show) {
                GUI.color = new Color(1, 1, 1, Mathf.PingPong(Time.time, Duration) / Duration);
                GUI.DrawTexture(
                                new Rect((float) ((Screen.width - Pic.width) * 0.5),
                                    (float) ((Screen.height - Pic.height) * 0.5), Pic.width, Pic.height), Pic);
            }
        }
    }
}