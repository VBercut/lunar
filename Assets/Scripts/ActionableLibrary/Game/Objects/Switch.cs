﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Objects.Items;
using UnityEngine;

namespace Assets.Scripts.Game.Objects
{
	class Switch : IntItem {

	    private bool _e;

        void OnTriggerEnter(Collider other)
        {
            if (!_e) {
                Fire(ItemEvents.ACTION, this, ItemEvents.ACTION);
                _e = true;
            }

        }

	}
}
