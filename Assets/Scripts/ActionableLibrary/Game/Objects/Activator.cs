﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Game.Objects {
    public class Activator : MonoBehaviour {

        public GameObject InteractObject;

        public List<GameObject> InteractObjects;

        public List<string> Tags;

        private Dictionary<string, int> _dictionaryTags; 

        // Use this for initialization
        void Start () {

            if (InteractObject != null) {
                InteractObject.SetActive(false);
            }

            if (InteractObjects != null) {
                foreach (var interactObject in InteractObjects) {
                    interactObject.SetActive(false);
                }
            }

            BuildDictionaryTags();
        }
	
        // Update is called once per frame
        void Update () {
            BuildDictionaryTags();
        }

        private void BuildDictionaryTags() {
            if (Tags == null) return;
            if (_dictionaryTags == null) _dictionaryTags = new Dictionary<string, int>();
            if (_dictionaryTags.Count == Tags.Count) return;

            _dictionaryTags = new Dictionary<string, int>();

            for (int i = 0; i < Tags.Count; i++) {
                _dictionaryTags[Tags[i]] = i;
            }
        }

        void OnTriggerEnter(Collider other) {
            SwitchObjects(other, true);
        }

        void OnTriggerExit(Collider other) {
            SwitchObjects(other, false);
        }

        private void SwitchObjects(Collider other, bool active) {
            if (!_dictionaryTags.ContainsKey(other.tag)) return;

            if (InteractObject != null) {
                InteractObject.SetActive(active);
            }

            if (InteractObjects != null) {
                foreach (var interactObject in InteractObjects) {
                    interactObject.SetActive(active);
                }
            }
        }

    }
}
