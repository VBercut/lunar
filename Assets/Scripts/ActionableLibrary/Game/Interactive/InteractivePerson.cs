﻿using Assets.ActionableLibrary;
using Assets.ActionableLibrary.Objects.Core;
using Assets.Scripts.ActionableLibrary.Objects.Core;
using Assets.Scripts.Objects.Core;
using UnityEngine;

namespace Assets.Scripts.Game.Interactive {
    public class InteractivePerson : MainInteractiveObject {

        public MainInteractiveObject InteractableInteractiveObject;

        public Collider CurrentCollider;

        public class Keys {
            public const string Interact = "Interact";
        }

        // Use this for initialization
        void Start () {
	
        }
	
        // Update is called once per frame
        void Update () {
            if (Input.GetButtonDown(Keys.Interact)) {
                Fire(PlayerActions.Action, this, PlayerActions.Action);
                InteractObject(PlayerActions.Action);
            }
                
        }

        private void InteractObject(string eventType) {
            if(InteractableInteractiveObject == null) return;

            InteractableInteractiveObject.Send(eventType);
        }

        private void InteractObject<T>(string eventType, T arg) {
            if (InteractableInteractiveObject == null) return;
            InteractableInteractiveObject.Send(eventType, arg);
        }

        private void InteractObject<T, U>(string eventType, T arg1, U arg2) {
            if (InteractableInteractiveObject == null) return;
            InteractableInteractiveObject.Send(eventType, arg1, arg2);
        }

        private void InteractObject<T, U, V>(string eventType, T arg1, U arg2, V arg3) {
            if (InteractableInteractiveObject == null) return;
            InteractableInteractiveObject.Send(eventType, arg1, arg2, arg3);
        }


        void OnTriggerEnter(Collider other) {
            var interactiveItem = other.GetComponent<InteractiveItem>();

            if (interactiveItem == null) return;

            if (InteractableInteractiveObject == null) InteractableInteractiveObject = interactiveItem;
        }

        void OnTriggerExit(Collider other) {
            var interactiveItem = other.GetComponent<InteractiveItem>();

            if (interactiveItem == null) return;

            if (InteractableInteractiveObject == interactiveItem) InteractableInteractiveObject = null;
        }
    }
}
