﻿using Assets.ActionableLibrary;
using Assets.ActionableLibrary.Objects.Core;
using Assets.Scripts.ActionableLibrary.Objects.Core;
using Assets.Scripts.Objects.Core;
using UnityEngine;

namespace Assets.Scripts.Game.Interactive {
    class InteractiveItem : MainInteractiveObject {

        public GameObject Reason;

        // Use this for initialization
        private void Start() {
            AddListener(PlayerActions.Action, OnAction);
        }

        protected virtual void OnAction() {
            UnityEngine.Debug.Log("Int item: ");

            if (Reason != null) {
                Reason.SetActive(true);
            } 
        }
    }
}
