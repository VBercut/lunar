﻿using Assets.ActionableLibrary;
using Assets.ActionableLibrary.Objects.Core;
using Assets.ActionableLibrary.Objects.Helpers;
using Assets.Scripts.ActionableLibrary.Objects.Core;
using Assets.Scripts.Game.GUIX;
using Assets.Scripts.Objects.Core;
using Assets.Scripts.Objects.Helpers;
using Assets.Scripts.Objects.Items;
using UnityEngine;

namespace Assets.Scripts.Objects {
    internal class Player : MainInteractiveObject {
        public Laser Laser = null;

        private int _count;
        private Interactive _currentItem;

        private ImageLabel _imageLabel;

        private void Start() {
            Laser.AddListener<Interactive>(Laser.NewItem, OnNewItem);
            _currentItem = null;
            Screen.showCursor = false;

            _imageLabel = GetComponent<ImageLabel>();
        }

        private void Update() {
            if (Input.GetButtonDown("Fire1"))
                Fire(PlayerActions.Action, this, PlayerActions.Action);

            _imageLabel.Show = (_currentItem != null);
        }

        private void OnNewItem(Interactive item)
        {
            if (_currentItem != null)
                _currentItem.Disconnect(this);

            if (_currentItem != item) {
                _currentItem = item;
                if (_currentItem != null) {
                    _currentItem.Connect(this);
                }
            }
        }
    }
}