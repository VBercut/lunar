﻿using UnityEngine;

namespace Assets.Scripts.Objects.Utils {
    public class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour {
        private static T instance;

        public static T Instance {
            get {
                if (instance == null) {
                    instance = (T) FindObjectOfType(typeof (T));


                    if (instance == null) {
                        var go = new GameObject();

                        go.hideFlags = HideFlags.HideAndDontSave;

                        instance = go.AddComponent<T>();
                    }
                }


                return instance;
            }
        }
    }
}