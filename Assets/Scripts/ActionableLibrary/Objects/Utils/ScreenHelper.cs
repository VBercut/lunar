﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Objects.Core;
using UnityEngine;

namespace Assets.Scripts.Objects.Utils
{
	class ScreenHelper : SingletonMonoBehaviour<ScreenHelper> {

	    public const string ScreenCHANGE = "screenChange";

	    private int _width;
	    private int _height;

        private void Start() {
            _width = Screen.width;
            _height = Screen.height;
        }

        private void Update() {
            if (_width != Screen.width || _height != Screen.height) {
                Messenger.Broadcast(ScreenCHANGE);
            }
        }
	}
}
