﻿using Assets.ActionableLibrary.Objects.Core;
using Assets.ActionableLibrary.Objects.Items;
using Assets.Scripts.ActionableLibrary.Objects.Core;
using Assets.Scripts.ActionableLibrary.Objects.Items;
using Assets.Scripts.Objects.Helpers;
using UnityEngine;

namespace Assets.ActionableLibrary.Objects.Helpers {
    internal class Laser : MainInteractiveObject {
        public const string NewItem = "newItem";
        public bool Cast = true;

        protected GameObject CurrentItem;
        public float Distance = 15f;

        protected IRayCaster RayCaster;

        private void Start() {
            RayCaster = new CenterRayCaster();
        }

        private void Update() {
            if (RayCaster != null) {
                RaycastHit hit = RayCaster.RayCastItem(Distance);

                GameObject newItem = hit.collider != null ? hit.collider.gameObject : null;

                if (CurrentItem != newItem) {
                    var interactiveObject = newItem != null ? newItem.GetComponent<ActionableInteractiveObject>() : null;

                    Fire(NewItem, interactiveObject);

                    CurrentItem = newItem;
                }
            }
        }

        public void ChangeCaster(IRayCaster caster) {
            if (caster != null) {
                RayCaster = caster;
            }
        }

        public Ray GetRay() {
            return RayCaster.GetRay();
        }
    }
}