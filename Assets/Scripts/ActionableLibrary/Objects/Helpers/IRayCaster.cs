﻿using UnityEngine;

namespace Assets.Scripts.Objects.Helpers {
    internal interface IRayCaster {
        RaycastHit RayCastItem();
        RaycastHit RayCastItem(float distance);
        Ray GetRay();
    }
}