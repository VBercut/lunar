﻿using Assets.Scripts.Objects.Core;
using Assets.Scripts.Objects.Helpers;
using Assets.Scripts.Objects.Utils;
using UnityEngine;

namespace Assets.ActionableLibrary.Objects.Helpers {
    internal class CenterRayCaster : IRayCaster {
        private Vector3 _castVector;
        private int _horizontalWidth;
        private int _verticalWidth;

        private Ray _ray;

        public CenterRayCaster() {
            _horizontalWidth = Screen.width / 2;
            _verticalWidth = Screen.height / 2;

            _castVector = new Vector3(_horizontalWidth, _verticalWidth);

            _ray = Camera.main.ScreenPointToRay(_castVector);

            Messenger.AddListener(ScreenHelper.ScreenCHANGE, OnScreenChange);
        }

        #region IRayCaster Members

        public RaycastHit RayCastItem() {
            return RayCastItem(10);
        }

        public RaycastHit RayCastItem(float distance) {
            RaycastHit hit;

            _ray = Camera.main.ScreenPointToRay(_castVector);

            Physics.Raycast(_ray, out hit, distance);

            return hit;
        }

        public Ray GetRay() {
            return _ray;
        }

        #endregion

        private void OnScreenChange() {
            _castVector.x = _horizontalWidth = Screen.width / 2;
            _castVector.y = _verticalWidth = Screen.height / 2;
        }
    }
}