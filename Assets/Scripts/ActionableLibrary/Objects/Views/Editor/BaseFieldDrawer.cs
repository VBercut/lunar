﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.ActionableLibrary.Objects.Core;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.ActionableLibrary.Objects.Views.Editor
{
    [CustomPropertyDrawer(typeof(BaseField))]
    class BaseFieldDrawer : PropertyDrawer {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.
            EditorGUI.BeginProperty(position, label, property);

            // Draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            // Don't make child fields be indented
            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            // Calculate rects
            //Rect amountRect = new Rect(position.x, position.y, 30, position.height);
            //Rect unitRect = new Rect(position.x + 35, position.y, 50, position.height);

            // Draw fields - passs GUIContent.none to each so they are drawn without labels
            //EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("Name"), GUIContent.none);
            //EditorGUI.PropertyField(unitRect, property.FindPropertyRelative("unit"), GUIContent.none);

            // Set indent back to what it was
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }
    }
}
