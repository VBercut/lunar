﻿using Assets.ActionableLibrary.Objects.Helpers;
using UnityEngine;

namespace Assets.ActionableLibrary.Objects.Views {
    internal class LaserRender : MonoBehaviour {
        public Laser Laser;

        public Color LaserColor = Color.red;
        public bool Show = true;

        private void Start() {
            //Laser = null;
        }

        private void Update() {
            if (Show) {
                Ray ray = Laser.GetRay();

                Debug.DrawRay(ray.origin, ray.direction * Laser.Distance, LaserColor);
            }
        }
    }
}