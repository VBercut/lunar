﻿using System.Collections.Generic;
using Assets.ActionableLibrary.Objects.Core;
using Assets.ActionableLibrary.Objects.Core.FSM;
using Assets.ActionableLibrary.Objects.States;
using Assets.Scripts.ActionableLibrary.Objects.Core;
using Assets.Scripts.ActionableLibrary.Objects.States;

namespace Assets.Scripts.ActionableLibrary.Objects.Items {
    public class ActionableInteractiveObject : MainInteractiveObject {

        public string[] StateNames;

        private FiniteStateMachineExt<ActionableInteractiveObject, ActionableObjectState> _fsm;

        private Dictionary<string, ActionableObjectState> _stateDictionary;

        public FiniteStateMachineExt<ActionableInteractiveObject, ActionableObjectState> Fsm {
            get {

                if (_fsm == null) {
                    _fsm = new FiniteStateMachineExt<ActionableInteractiveObject, ActionableObjectState>(this);

                    _stateDictionary = new Dictionary<string, ActionableObjectState>();

                    _states = GetComponents<ActionableObjectState>();

                    int stateIndex = 0;

                    foreach (var stateName in StateNames) {

                        var currentState = _states[stateIndex];

                        _fsm.RegisterState(currentState);
                        _stateDictionary[stateName] = currentState;

                        stateIndex++;
                    }
                }

                return _fsm;
            }
        }

        private ActionableObjectState[] _states;

        private ActionableObjectState DummyFirstState {
            get {
                if(_states == null) return null;

                return _states[0];
            }
        }


        public ActionableObjectState FirstState;
        public ActionableObjectState CurrentState;


        public FSMStateExt<ActionableInteractiveObject, ActionableObjectState> PreviousState() {
            return _fsm.PreviousState();
        }

        protected virtual void Start() {
            Fsm.ChangeState(FirstState ?? DummyFirstState);
        }

        protected virtual void Update() {
            Fsm.Update();

            CurrentState = Fsm.CurrentState() as ActionableObjectState;
        }

        public void ChangeState(string stateName) {
            Fsm.ChangeState(_stateDictionary[stateName]);
        }
    }
}
