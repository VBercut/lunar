﻿using System.Collections;
using Assets.Scripts.Objects.Core;
using Assets.Scripts.Objects.States.Doors;
using UnityEngine;

namespace Assets.Scripts.Objects.Items {
    public class Door : IntItem {
        #region States enum

        public enum States {
            Open,
            Close,
            Opening
        }

        #endregion

        private CloseDoorState _closeDoorState;

        private FiniteStateMachine<Door, States> _fsm;

        private OpenDoorState _openDoorState;
        private OpeningDoorState _openingDoorState;


        private void Start() {
            //Initialise the state machine
            _fsm = new FiniteStateMachine<Door, States>(this);

            _openDoorState = new OpenDoorState();
            _closeDoorState = new CloseDoorState();
            _openingDoorState = new OpeningDoorState();

            _fsm.RegisterState(_openDoorState);
            _fsm.RegisterState(_closeDoorState);
            _fsm.RegisterState(_openingDoorState);

            ChangeState(_closeDoorState.StateID);
        }


        private void Update() {
            _fsm.Update();
        }


        protected override void OnAction(MonoBehaviour from, string type) {
            var door = _fsm.CurrentState() as IDoor;

            if (door != null) {
                door.OnAction(from, type);
            }
        }

        public void ChangeState(States newState)
        {
            _fsm.ChangeState(newState);
        }

        public void ChangeState(States newState, float delay)
        {
            StartCoroutine(ChangeStateWithDelay(newState, delay));
        }

        private IEnumerator ChangeStateWithDelay(States newState, float delay)
        {
            yield return new WaitForSeconds(delay);
            ChangeState(newState);
        }

        public FSMState<Door, States> PreviousState()
        {
            return _fsm.PreviousState();
        }
    }
}