﻿using System.Collections;
using System.Collections.Generic;
using Assets.ActionableLibrary.Objects.Core;
using Assets.Scripts.ActionableLibrary.Objects.Core;
using Assets.Scripts.Objects.Core;
using Assets.Scripts.Objects.Items;
using Assets.Scripts.Objects.States.Doors;
using Assets.Scripts.Objects.States.Ladders;
using UnityEngine;

namespace Assets.ActionableLibrary.Objects.Items {
    public class AnimateLadder : IntItem {
        #region States enum

        public enum States {
            Open,
            Close,
            Opening
        }

        #endregion

        [HideInInspector] public List<MeshRenderer> ChildrenMeshes;
        [HideInInspector] public List<Transform> ChildrenPositions;
        public float Duration = 3.3f;
        public iTween.EaseType EaseType = iTween.EaseType.easeOutExpo;
        public GameObject FirstStep;
        [HideInInspector] public float FullLenght;
        [HideInInspector] public float LadderHeight;
        [HideInInspector] public float LadderLenght;
        public GameObject Slope;
        public float StartDelay = 0.3f;
        public GameObject Steps;
        public MainInteractiveObject Switch;

        public bool Upper = true;

        private FiniteStateMachine<AnimateLadder, States> _fsm;

        public float StepWidth {
            get {
                var firstMesh = FirstStep.GetComponent<Transform>();
                var firstColider = FirstStep.GetComponent<BoxCollider>();
                return firstMesh.localScale.x * firstColider.size.x;
            }
        }

        public float StepHeight {
            get {
                var firstMesh = FirstStep.GetComponent<Transform>();
                var firstColider = FirstStep.GetComponent<BoxCollider>();
                return firstMesh.localScale.y * firstColider.size.y;
            }
        }


        public float StepLenght
        {
            get
            {
                var firstMesh = FirstStep.GetComponent<Transform>();
                var firstColider = FirstStep.GetComponent<BoxCollider>();
                return firstMesh.localScale.z * firstColider.size.z;
            }
        }

        public float StepSlope
        {
            get {
                return Mathf.Sqrt(StepWidth * StepWidth + StepHeight * StepHeight);              
            }
        }

        public float Angle {
            get {
                float a = Mathf.Asin(StepHeight / StepSlope) * Mathf.Rad2Deg;

                if (!Upper)
                    return -a;

                return a;
            }
        }

        public float SlopeLenght {
            get {
                return StepSlope * ChildrenPositions.Count;
            }
        }

        public float AddSlopeLenght
        {
            get
            {
                return Mathf.Sqrt(4 * StepHeight * StepHeight - SlopeHeight * SlopeHeight);
            }
        }

        public float SlopeHeight
        {
            get
            {
                return 2 * StepWidth * StepHeight / StepSlope;
            }
        }




        private void Start() {
            _fsm = new FiniteStateMachine<AnimateLadder, States>(this);

            _fsm.RegisterState(new OpenLadderState());
            _fsm.RegisterState(new CloseLadderState());
            _fsm.RegisterState(new OpeningLadderState());

            ChangeState(States.Close);

            if (Switch == null) {
                ChangeState(States.Opening);
            } else {
                Connect(Switch);
            }

        }


        public void AssambleLadder() {
            Transform steps = transform.FindChild("Steps");

            ChildrenMeshes = new List<MeshRenderer>();
            ChildrenPositions = new List<Transform>();

            foreach (Transform step in steps.transform) {
                ChildrenMeshes.Add(step.gameObject.GetComponent<MeshRenderer>());
                ChildrenPositions.Add(step);
            }

            Debug.Log("Количество ступеней: " + ChildrenPositions.Count);

            Debug.Log("Габариты ступени: \n" + "Ширина: " + StepWidth + " Высота: " + StepHeight
            + " Длина: " + StepLenght);

            Debug.Log("Габариты лестницы: \n" + "Ширина: " + (StepWidth * ChildrenPositions.Count) + " Высота: " + (StepHeight * ChildrenPositions.Count)
                + " Длина: " + StepLenght);

            Debug.Log("Угол наклона: " + Angle);

            transform.localScale = Vector3.one;

            var slopeCollider = Slope.GetComponent<BoxCollider>();

            slopeCollider.size = new Vector3(SlopeLenght, SlopeHeight, StepLenght);

            float newCenterY = - SlopeHeight / 2;

            slopeCollider.center = new Vector3(SlopeLenght / 2, newCenterY);

            DownLadder();
        }

        public void Up() {
            int c = ChildrenPositions.Count;

            for (int i = 0; i < c; i++) {
                Transform p = ChildrenPositions[i];
                float newY = Upper ? i * StepHeight : i * StepHeight * -1;
                p.localPosition = new Vector3(p.localPosition.x, newY, 0);
            }
            
            Slope.transform.Rotate(Vector3.forward, Angle, Space.Self);
        }

        public void DownLadder() {
            for (int i = 0; i < ChildrenPositions.Count; i++) {
                ChildrenPositions[i].localPosition = new Vector3(i * StepWidth, 0, 0);
            }

            float slopeX = Upper ? - 1.5f * StepWidth  : StepWidth / 2;
            float slopeY = Upper ? - StepHeight / 2 : StepHeight / 2;

            Slope.transform.localPosition = new Vector3(slopeX, slopeY, 0);
            Slope.transform.localRotation = new Quaternion();
            Slope.transform.localScale = Vector3.one;
        }

        public void Animate() {

            iTween.RotateTo(Slope, iTween.Hash("z", Angle, "islocal", true, "time", Duration, "easeType", EaseType.ToString(), "delay", StartDelay));

            for (int i = 0; i < ChildrenPositions.Count; i++)
            {
                Transform currentChild = ChildrenPositions[i];
                float newY = Upper ? i * StepHeight : i * StepHeight * -1;
                iTween.MoveBy(currentChild.gameObject, iTween.Hash("y", newY,
                    "time", Duration, "easeType", EaseType.ToString(), "delay", StartDelay));
            }

            ChangeState(States.Open);
        }

        private void Update() {
            _fsm.Update();
        }


        protected override void OnConnect(IDispatcher from)
        {
            from.AddListener<MonoBehaviour, string>(ItemEvents.ACTION, OnAction);
        }

        protected override void OnDisconnect(IDispatcher from)
        {
            from.RemoveListener<MonoBehaviour, string>(ItemEvents.ACTION, OnAction);
        }

        protected override void OnAction(MonoBehaviour from, string type) {
            Debug.Log(type);

            var door = _fsm.CurrentState() as IDoor;

            if (door != null) {
                door.OnAction(from, type);
            }
        }


        public void ChangeState(States newState) {
            _fsm.ChangeState(newState);
        }

        public void ChangeState(States newState, float delay) {
            StartCoroutine(ChangeStateWithDelay(newState, delay));
        }

        private IEnumerator ChangeStateWithDelay(States newState, float delay) {
            yield return new WaitForSeconds(delay);
            ChangeState(newState);
        }

        public FSMState<AnimateLadder, States> PreviousState() {
            return _fsm.PreviousState();
        }
    }
}