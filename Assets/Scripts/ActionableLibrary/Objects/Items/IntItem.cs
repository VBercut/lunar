﻿using Assets.ActionableLibrary;
using Assets.ActionableLibrary.Objects.Core;
using Assets.Scripts.ActionableLibrary.Objects.Core;
using Assets.Scripts.Objects.Core;
using UnityEngine;

namespace Assets.Scripts.Objects.Items {
    public class IntItem : MainInteractiveObject {
        public Color HoverColor = Color.red;
        public Color StartColor = Color.white;

        protected override void OnConnect(IDispatcher from) {
            from.AddListener<MonoBehaviour, string>(PlayerActions.Action, OnAction);
            renderer.material.color = HoverColor;
        }

        protected override void OnDisconnect(IDispatcher from) {
            from.RemoveListener<MonoBehaviour, string>(PlayerActions.Action, OnAction);
            renderer.material.color = StartColor;
        }

        protected virtual void OnAction(MonoBehaviour from, string type) {
            if (type == PlayerActions.Action) {
                Debug.Log("Oh, no!");
                var player = from.GetComponent<Player>();
                if (player != null) {
                    Ray ray = player.Laser.GetRay();
                    rigidbody.AddForce(ray.direction * 1000);
                }
            }
        }
    }
}