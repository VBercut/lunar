﻿using Assets.ActionableLibrary.Objects.Items;
using Assets.Scripts.Objects.Core;
using Assets.Scripts.Objects.Items;
using Assets.Scripts.Objects.States.Doors;
using UnityEngine;

namespace Assets.Scripts.Objects.States.Ladders {
    public class CloseLadderState : FSMState<AnimateLadder, AnimateLadder.States>, IDoor
    {
        public override AnimateLadder.States StateID
        {
            get { return AnimateLadder.States.Close; }
        }

        #region IDoor Members

        public void OnAction(MonoBehaviour from, string type) {
            if (type == ItemEvents.ACTION)
            {
                Entity.ChangeState(AnimateLadder.States.Opening);
                from.GetComponent<AudioSource>().Play();
            }
        }

        #endregion

        public override void Enter() {
            Debug.Log("I'm opened!");
            Entity.AssambleLadder();
        }
    }
}