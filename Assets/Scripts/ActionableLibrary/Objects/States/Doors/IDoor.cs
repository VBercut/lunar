﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Objects.States.Doors
{
    public interface IDoor {
        void OnAction(MonoBehaviour from, string type);
    }
}
