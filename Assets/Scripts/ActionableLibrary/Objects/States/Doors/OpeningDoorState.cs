﻿using System.Collections;
using Assets.Scripts.Objects.Core;
using Assets.Scripts.Objects.Items;
using UnityEngine;

namespace Assets.Scripts.Objects.States.Doors {
    public class OpeningDoorState : FSMState<Door, Door.States>, IDoor {
        public override Door.States StateID {
            get { return Door.States.Opening; }
        }

        #region IDoor Members

        public void OnAction(MonoBehaviour from, string type) {}

        #endregion

        public override void Enter() {
            PreviousState = Entity.PreviousState();

            Entity.StartCoroutine(PlayAnimation(PreviousState.StateID));
        }

        private IEnumerator PlayAnimation(Door.States state) {
            if (state == Door.States.Close) {
                Entity.animation["OpeningDoor"].speed = 1.0f;
            }
            if (state == Door.States.Open) {
                Entity.animation["OpeningDoor"].speed = -1.0f;
                Entity.animation["OpeningDoor"].time = Entity.animation["OpeningDoor"].length;
            }

            Entity.animation.Play("OpeningDoor");

            yield return new WaitForSeconds(Entity.animation["OpeningDoor"].length);

            if (state == Door.States.Close) {
                Entity.ChangeState(Door.States.Open);
            }
            if (state == Door.States.Open) {
                Entity.ChangeState(Door.States.Close);
            }
            yield return null;
        }

        public override void Execute() {}

        public override void Exit() {}
    }
}