﻿using Assets.ActionableLibrary;
using Assets.Scripts.Objects.Core;
using Assets.Scripts.Objects.Items;
using UnityEngine;

namespace Assets.Scripts.Objects.States.Doors {
    public class OpenDoorState : FSMState<Door, Door.States>, IDoor {
        public override Door.States StateID {
            get { return Door.States.Open; }
        }

        #region IDoor Members

        public void OnAction(MonoBehaviour from, string type) {
            if (type == PlayerActions.Action) {
                Entity.ChangeState(Door.States.Opening);
            }
        }

        #endregion

        public override void Enter() {
            Debug.Log("I'm opened!");
        }
    }
}