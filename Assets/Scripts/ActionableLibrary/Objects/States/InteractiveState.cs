﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.ActionableLibrary.Objects.States
{
    public interface InteractiveState<T, U> {

        InteractiveState<T, U> PreviousState { get; }
        U StateID { get; }

        void RegisterState(T entity);

        void Enter();

        void Execute();

        void Exit();
    }
}
