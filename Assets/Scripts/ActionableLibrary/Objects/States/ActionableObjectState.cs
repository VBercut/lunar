﻿using Assets.ActionableLibrary.Objects.Core.FSM;
using Assets.Scripts.ActionableLibrary.Objects.Items;
using UnityEngine;

namespace Assets.Scripts.ActionableLibrary.Objects.States {
    public class ActionableObjectState : MonoBehaviour, FSMStateExt<ActionableInteractiveObject, ActionableObjectState> {
        public FSMStateExt<ActionableInteractiveObject, ActionableObjectState> PreviousState {
            get { return Entity.Fsm.PreviousState(); }
        }

        public ActionableObjectState StateID { get { return this; } }

        protected ActionableInteractiveObject Entity;

        public void RegisterState(ActionableInteractiveObject entity) {
            Entity = entity;
        }

        public virtual void Enter() { }

        public virtual void Execute() { }

        public virtual void Exit() { }
    }
}
