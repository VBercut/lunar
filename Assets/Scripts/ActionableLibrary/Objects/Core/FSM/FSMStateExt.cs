﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.ActionableLibrary.Objects.Core.FSM
{
    public interface FSMStateExt<T, U> {

        FSMStateExt<T, U> PreviousState { get; }
        U StateID { get; }

        void RegisterState(T entity);

        void Enter();

        void Execute();

        void Exit();
    }
}
