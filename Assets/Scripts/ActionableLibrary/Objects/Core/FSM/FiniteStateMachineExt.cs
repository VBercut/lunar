﻿using System;
using System.Collections.Generic;

namespace Assets.ActionableLibrary.Objects.Core.FSM {
    public class FiniteStateMachineExt<T, U> {

        private T Owner;
        private Dictionary<U, FSMStateExt<T, U>> stateRef;
        private FSMStateExt<T, U> _currentState;
        private FSMStateExt<T, U> _globalState;
        private FSMStateExt<T, U> _previousState;

        public FiniteStateMachineExt(T owner)
        {
            Owner = owner;
            stateRef = new Dictionary<U, FSMStateExt<T, U>>();
        }

        public void Awake() {
            _currentState = null;
            _previousState = null;
            _globalState = null;
        }

        public void Update() {
            if (_globalState != null) _globalState.Execute();
            if (_currentState != null) _currentState.Execute();
        }

        public void ChangeState(FSMStateExt<T, U> NewState)
        {
            _previousState = _currentState;

            if (_currentState != null)
                _currentState.Exit();

            _currentState = NewState;

            if (_currentState != null)
                _currentState.Enter();
        }

        public void RevertToPreviousState() {
            if (_previousState != null)
                ChangeState(_previousState);
        }

        //Changing state via enum
        public void ChangeState(U stateID) {
            try {
                FSMStateExt<T, U> state = stateRef[stateID];
                ChangeState(state);
            }
            catch (KeyNotFoundException) {
                throw new Exception("There is no State assiciated with that definition");
            }
        }

        public FSMStateExt<T, U> RegisterState(FSMStateExt<T, U> state)
        {
            state.RegisterState(Owner);
            stateRef.Add(state.StateID, state);
            return state;
        }

        public void UnregisterState(FSMStateExt<T, U> state)
        {
            stateRef.Remove(state.StateID);
        }

        public FSMStateExt<T, U> CurrentState()
        {
            return _currentState;
        }

        public FSMStateExt<T, U> PreviousState()
        {
            return _previousState;
        }

    }
}
