using System;

namespace Assets.Scripts.Objects.Core {
    public class FSMState <T, U>   {
	
        protected T Entity;

        protected FSMState<T, U> PreviousState;
	
        public void RegisterState(T entity)
        {
            this.Entity = entity;
        }
	
        virtual public U StateID 
        {
            get{
                throw new ArgumentException("State ID not spicified in child class");
            }
        }

        virtual public void Enter (){}
		
        virtual public void Execute (){}

        virtual public void Exit(){}
    }
}
