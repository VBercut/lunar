using System;
using System.Collections.Generic;

namespace Assets.Scripts.Objects.Core {
    public class FiniteStateMachine<T, U> {
        private T Owner;
        private Dictionary<U, FSMState<T, U>> stateRef;
        private FSMState<T, U> _currentState;
        private FSMState<T, U> _globalState;
        private FSMState<T, U> _previousState;

        public FiniteStateMachine(T owner) {
            Owner = owner;
            stateRef = new Dictionary<U, FSMState<T, U>>();
        }

        public void Awake() {
            _currentState = null;
            _previousState = null;
            _globalState = null;
        }

        public void Update() {
            if (_globalState != null) _globalState.Execute();
            if (_currentState != null) _currentState.Execute();
        }

        public void ChangeState(FSMState<T, U> NewState) {
            _previousState = _currentState;

            if (_currentState != null)
                _currentState.Exit();

            _currentState = NewState;

            if (_currentState != null)
                _currentState.Enter();
        }

        public void RevertToPreviousState() {
            if (_previousState != null)
                ChangeState(_previousState);
        }

        //Changing state via enum
        public void ChangeState(U stateID) {
            try {
                FSMState<T, U> state = stateRef[stateID];
                ChangeState(state);
            }
            catch (KeyNotFoundException) {
                throw new Exception("There is no State assiciated with that definition");
            }
        }

        public FSMState<T, U> RegisterState(FSMState<T, U> state) {
            state.RegisterState(Owner);
            stateRef.Add(state.StateID, state);
            return state;
        }

        public void UnregisterState(FSMState<T, U> state) {
            stateRef.Remove(state.StateID);
        }

        public FSMState<T, U> CurrentState() {
            return _currentState;
        }

        public FSMState<T, U> PreviousState()
        {
            return _previousState;
        }
    };
}