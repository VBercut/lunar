﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.ActionableLibrary.Objects.Core {

    [Serializable]
    public class BaseField : Object {

        private readonly String _name;
        public object Value;

        public BaseField(String name) : this(name, null) {
        }

        public BaseField(String name, object value) {
            _name = name;
            Value = value;
        }

        public string Name {
            get { return _name; }
        }
    }
}
