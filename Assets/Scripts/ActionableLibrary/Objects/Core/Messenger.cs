﻿/*
 * Advanced C# messenger by Ilya Suzdalnitski. V1.0
 * 
 * Based on Rod Hyde's "CSharpMessenger" and Magnus Wolffelt's "CSharpMessenger Extended".
 * 
 * Features:
 	* Prevents a MissingReferenceException because of a reference to a destroyed message handler.
 	* Option to log all messages
 	* Extensive error detection, preventing silent bugs
 * 
 * Usage examples:
 	1. Messenger.AddListener<GameObject>("prop collected", PropCollected);
 	   Messenger.Broadcast<GameObject>("prop collected", prop);
 	2. Messenger.AddListener<float>("speed changed", SpeedChanged);
 	   Messenger.Broadcast<float>("speed changed", 0.5f);
 * 
 * Messenger cleans up its evenTable automatically upon loading of a new level.
 * 
 * Don't forget that the messages that should survive the cleanup, should be marked with Messenger.MarkAsPermanent(string)
 * 
 */

//#define LOG_ALL_MESSAGES
//#define LOG_ADD_LISTENER
//#define LOG_BROADCAST_MESSAGE

//#define REQUIRE_LISTENER

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Objects.Core {
    internal static class Messenger {
        #region Internal variables

        //Disable the unused variable warning
#pragma warning disable 0414
        //Ensures that the MessengerHelper will be created automatically upon start of the game.
        private static MessengerHelper _messengerHelper =
            (new GameObject("MessengerHelper")).AddComponent<MessengerHelper>();
#pragma warning restore 0414

        public static Dictionary<string, Delegate> EventTable = new Dictionary<string, Delegate>();

        public static Dictionary<IDispatcher, Dictionary<string, Delegate>> ObserverTable =
            new Dictionary<IDispatcher, Dictionary<string, Delegate>>();

        //Message handlers that should never be removed, regardless of calling Cleanup
        public static List<string> PermanentMessages = new List<string>();

        #endregion

        #region Helper methods

        //Marks a certain message as permanent.
        public static void MarkAsPermanent(string eventType) {
#if LOG_ALL_MESSAGES
		Debug.Log("Messenger MarkAsPermanent \t\"" + eventType + "\"");
#endif

            PermanentMessages.Add(eventType);
        }


        public static void Cleanup() {
#if LOG_ALL_MESSAGES
		Debug.Log("MESSENGER Cleanup. Make sure that none of necessary listeners are removed.");
#endif

            var messagesToRemove = new List<string>();

            foreach (var pair in EventTable) {
                bool wasFound = false;

                foreach (string message in PermanentMessages) {
                    if (pair.Key == message) {
                        wasFound = true;
                        break;
                    }
                }

                if (!wasFound)
                    messagesToRemove.Add(pair.Key);
            }

            foreach (string message in messagesToRemove) {
                EventTable.Remove(message);
            }
        }

        public static void PrintEventTable() {
            Debug.Log("\t\t\t=== MESSENGER PrintEventTable ===");

            foreach (var pair in EventTable) {
                Debug.Log("\t\t\t" + pair.Key + "\t\t" + pair.Value);
            }

            Debug.Log("\n");
        }

        #endregion

        #region Message logging and exception throwing

        public static void OnListenerAdding(string eventType, Delegate listenerBeingAdded) {
#if LOG_ALL_MESSAGES || LOG_ADD_LISTENER
		Debug.Log("MESSENGER OnListenerAdding \t\"" + eventType + "\"\t{" + listenerBeingAdded.Target + " -> " + listenerBeingAdded.Method + "}");
#endif

            if (!EventTable.ContainsKey(eventType)) {
                EventTable.Add(eventType, null);
            }

            Delegate d = EventTable[eventType];
            if (d != null && d.GetType() != listenerBeingAdded.GetType()) {
                throw new ListenerException(
                    string.Format(
                                  "Attempting to add listener with inconsistent signature for event type {0}. Current listeners have type {1} and listener being added has type {2}",
                        eventType, d.GetType().Name, listenerBeingAdded.GetType().Name));
            }
        }


        public static void OnListenerAdding(IDispatcher dispatcher, string eventType, Delegate listenerBeingAdded) {
#if LOG_ALL_MESSAGES || LOG_ADD_LISTENER
		Debug.Log("MESSENGER OnListenerAdding \t\"" + eventType + "\"\t{" + listenerBeingAdded.Target + " -> " + listenerBeingAdded.Method + "}");
#endif

            if (!ObserverTable.ContainsKey(dispatcher)) {
                ObserverTable.Add(dispatcher, new Dictionary<string, Delegate>());
            }

            Dictionary<string, Delegate> eventTable = ObserverTable[dispatcher];

            if (!eventTable.ContainsKey(eventType)) {
                eventTable.Add(eventType, null);
            }

            Delegate d = eventTable[eventType];
            if (d != null && d.GetType() != listenerBeingAdded.GetType()) {
                throw new ListenerException(
                    string.Format(
                                  "Attempting to add listener with inconsistent signature for event type {0}. Current listeners have type {1} and listener being added has type {2}",
                        eventType, d.GetType().Name, listenerBeingAdded.GetType().Name));
            }
        }


        public static void OnListenerRemoving(string eventType, Delegate listenerBeingRemoved) {
#if LOG_ALL_MESSAGES
		Debug.Log("MESSENGER OnListenerRemoving \t\"" + eventType + "\"\t{" + listenerBeingRemoved.Target + " -> " + listenerBeingRemoved.Method + "}");
#endif

            if (EventTable.ContainsKey(eventType)) {
                Delegate d = EventTable[eventType];

                if (d == null) {
                    throw new ListenerException(
                        string.Format(
                                      "Attempting to remove listener with for event type \"{0}\" but current listener is null.",
                            eventType));
                }
                else if (d.GetType() != listenerBeingRemoved.GetType()) {
                    throw new ListenerException(
                        string.Format(
                                      "Attempting to remove listener with inconsistent signature for event type {0}. Current listeners have type {1} and listener being removed has type {2}",
                            eventType, d.GetType().Name, listenerBeingRemoved.GetType().Name));
                }
            }
            else {
                throw new ListenerException(
                    string.Format(
                                  "Attempting to remove listener for type \"{0}\" but Messenger doesn't know about this event type.",
                        eventType));
            }
        }


        public static void OnListenerRemoving(IDispatcher dispatcher, string eventType, Delegate listenerBeingRemoved) {
#if LOG_ALL_MESSAGES
		Debug.Log("MESSENGER OnListenerRemoving \t\"" + eventType + "\"\t{" + listenerBeingRemoved.Target + " -> " + listenerBeingRemoved.Method + "}");
#endif
            Dictionary<string, Delegate> eventTable = ObserverTable[dispatcher];

            if (eventTable.ContainsKey(eventType)) {
                Delegate d = eventTable[eventType];

                if (d == null) {
                    throw new ListenerException(
                        string.Format(
                                      "Attempting to remove listener with for event type \"{0}\" but current listener is null.",
                            eventType));
                }
                else if (d.GetType() != listenerBeingRemoved.GetType()) {
                    throw new ListenerException(
                        string.Format(
                                      "Attempting to remove listener with inconsistent signature for event type {0}. Current listeners have type {1} and listener being removed has type {2}",
                            eventType, d.GetType().Name, listenerBeingRemoved.GetType().Name));
                }
            }
            else {
                throw new ListenerException(
                    string.Format(
                                  "Attempting to remove listener for type \"{0}\" but Messenger doesn't know about this event type.",
                        eventType));
            }
        }


        public static void OnListenerRemoved(string eventType) {
            if (EventTable[eventType] == null) {
                EventTable.Remove(eventType);
            }
        }

        public static void OnListenerRemoved(IDispatcher dispatcher, string eventType) {
            Dictionary<string, Delegate> eventTable = ObserverTable[dispatcher];

            if (eventTable[eventType] == null) {
                eventTable.Remove(eventType);
            }
        }

        public static void OnBroadcasting(string eventType) {
#if REQUIRE_LISTENER
        if (!eventTable.ContainsKey(eventType)) {
            throw new BroadcastException(
                string.Format(
                              "Broadcasting message \"{0}\" but no listener found. Try marking the message with Messenger.MarkAsPermanent.",
                    eventType));
        }
#endif
        }

        public static void OnBroadcasting(IDispatcher dispatcher, string eventType) {
            Dictionary<string, Delegate> eventTable = ObserverTable[dispatcher];
#if REQUIRE_LISTENER
        if (!eventTable.ContainsKey(eventType)) {
            throw new BroadcastException(
                string.Format(
                              "Broadcasting message \"{0}\" but no listener found. Try marking the message with Messenger.MarkAsPermanent.",
                    eventType));
        }
#endif
        }

        public static BroadcastException CreateBroadcastSignatureException(string eventType) {
            return
                new BroadcastException(
                    string.Format(
                                  "Broadcasting message \"{0}\" but listeners have a different signature than the broadcaster.",
                        eventType));
        }

        public class BroadcastException : Exception {
            public BroadcastException(string msg)
                : base(msg) {}
        }

        public class ListenerException : Exception {
            public ListenerException(string msg)
                : base(msg) {}
        }

        #endregion

        #region AddListener

        //No parameters
        public static void AddListener(string eventType, Callback handler) {
            OnListenerAdding(eventType, handler);
            EventTable[eventType] = (Callback) EventTable[eventType] + handler;
        }

        //Single parameter
        public static void AddListener<T>(string eventType, Callback<T> handler) {
            OnListenerAdding(eventType, handler);
            EventTable[eventType] = (Callback<T>) EventTable[eventType] + handler;
        }

        //Two parameters
        public static void AddListener<T, U>(string eventType, Callback<T, U> handler) {
            OnListenerAdding(eventType, handler);
            EventTable[eventType] = (Callback<T, U>) EventTable[eventType] + handler;
        }

        //Three parameters
        public static void AddListener<T, U, V>(string eventType, Callback<T, U, V> handler) {
            OnListenerAdding(eventType, handler);
            EventTable[eventType] = (Callback<T, U, V>) EventTable[eventType] + handler;
        }


        //No parameters
        public static void AddListener(IDispatcher dispatcher, string eventType, Callback handler) {
            OnListenerAdding(dispatcher, eventType, handler);
            Dictionary<string, Delegate> eventTable = ObserverTable[dispatcher];
            eventTable[eventType] = (Callback) eventTable[eventType] + handler;
        }

        //Single parameter
        public static void AddListener<T>(IDispatcher dispatcher, string eventType, Callback<T> handler) {
            OnListenerAdding(dispatcher, eventType, handler);
            Dictionary<string, Delegate> eventTable = ObserverTable[dispatcher];
            eventTable[eventType] = (Callback<T>) eventTable[eventType] + handler;
        }

        //Two parameters
        public static void AddListener<T, U>(IDispatcher dispatcher, string eventType, Callback<T, U> handler) {
            OnListenerAdding(dispatcher, eventType, handler);
            Dictionary<string, Delegate> eventTable = ObserverTable[dispatcher];
            eventTable[eventType] = (Callback<T, U>) eventTable[eventType] + handler;
        }

        //Three parameters
        public static void AddListener<T, U, V>(IDispatcher dispatcher, string eventType, Callback<T, U, V> handler) {
            OnListenerAdding(dispatcher, eventType, handler);
            Dictionary<string, Delegate> eventTable = ObserverTable[dispatcher];
            eventTable[eventType] = (Callback<T, U, V>) eventTable[eventType] + handler;
        }

        #endregion

        #region RemoveListener

        //No parameters
        public static void RemoveListener(string eventType, Callback handler) {
            OnListenerRemoving(eventType, handler);
            EventTable[eventType] = (Callback) EventTable[eventType] - handler;
            OnListenerRemoved(eventType);
        }

        //Single parameter
        public static void RemoveListener<T>(string eventType, Callback<T> handler) {
            OnListenerRemoving(eventType, handler);
            EventTable[eventType] = (Callback<T>) EventTable[eventType] - handler;
            OnListenerRemoved(eventType);
        }

        //Two parameters
        public static void RemoveListener<T, U>(string eventType, Callback<T, U> handler) {
            OnListenerRemoving(eventType, handler);
            EventTable[eventType] = (Callback<T, U>) EventTable[eventType] - handler;
            OnListenerRemoved(eventType);
        }

        //Three parameters
        public static void RemoveListener<T, U, V>(string eventType, Callback<T, U, V> handler) {
            OnListenerRemoving(eventType, handler);
            EventTable[eventType] = (Callback<T, U, V>) EventTable[eventType] - handler;
            OnListenerRemoved(eventType);
        }


        //No parameters
        public static void RemoveListener(IDispatcher dispatcher, string eventType, Callback handler) {
            OnListenerRemoving(dispatcher, eventType, handler);
            Dictionary<string, Delegate> eventTable = ObserverTable[dispatcher];
            eventTable[eventType] = (Callback) eventTable[eventType] - handler;
            OnListenerRemoved(dispatcher, eventType);
        }

        //Single parameter
        public static void RemoveListener<T>(IDispatcher dispatcher, string eventType, Callback<T> handler) {
            OnListenerRemoving(dispatcher, eventType, handler);
            Dictionary<string, Delegate> eventTable = ObserverTable[dispatcher];
            eventTable[eventType] = (Callback<T>) eventTable[eventType] - handler;
            OnListenerRemoved(dispatcher, eventType);
        }

        //Two parameters
        public static void RemoveListener<T, U>(IDispatcher dispatcher, string eventType, Callback<T, U> handler) {
            OnListenerRemoving(dispatcher, eventType, handler);
            Dictionary<string, Delegate> eventTable = ObserverTable[dispatcher];
            eventTable[eventType] = (Callback<T, U>) eventTable[eventType] - handler;
            OnListenerRemoved(dispatcher, eventType);
        }

        //Three parameters
        public static void RemoveListener<T, U, V>(IDispatcher dispatcher, string eventType, Callback<T, U, V> handler) {
            OnListenerRemoving(dispatcher, eventType, handler);
            Dictionary<string, Delegate> eventTable = ObserverTable[dispatcher];
            eventTable[eventType] = (Callback<T, U, V>) eventTable[eventType] - handler;
            OnListenerRemoved(dispatcher, eventType);
        }

        #endregion

        #region Broadcast

        //No parameters
        public static void Broadcast(string eventType) {
#if LOG_ALL_MESSAGES || LOG_BROADCAST_MESSAGE
		Debug.Log("MESSENGER\t" + System.DateTime.Now.ToString("hh:mm:ss.fff") + "\t\t\tInvoking \t\"" + eventType + "\"");
#endif
            OnBroadcasting(eventType);

            Delegate d;
            if (EventTable.TryGetValue(eventType, out d)) {
                var callback = d as Callback;

                if (callback != null) {
                    callback();
                }
                else {
                    throw CreateBroadcastSignatureException(eventType);
                }
            }
        }

        //Single parameter
        public static void Broadcast<T>(string eventType, T arg1) {
#if LOG_ALL_MESSAGES || LOG_BROADCAST_MESSAGE
		Debug.Log("MESSENGER\t" + System.DateTime.Now.ToString("hh:mm:ss.fff") + "\t\t\tInvoking \t\"" + eventType + "\"");
#endif
            OnBroadcasting(eventType);

            Delegate d;
            if (EventTable.TryGetValue(eventType, out d)) {
                try {
                    d.DynamicInvoke(arg1);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message);
                }
/*            var callback = d as Callback<T>;

            if (callback != null) {
                callback(arg1);
            }
            else {
                throw CreateBroadcastSignatureException(eventType);
            }*/
            }
        }

        //Two parameters
        public static void Broadcast<T, U>(string eventType, T arg1, U arg2) {
#if LOG_ALL_MESSAGES || LOG_BROADCAST_MESSAGE
		Debug.Log("MESSENGER\t" + System.DateTime.Now.ToString("hh:mm:ss.fff") + "\t\t\tInvoking \t\"" + eventType + "\"");
#endif
            OnBroadcasting(eventType);

            Delegate d;
            if (EventTable.TryGetValue(eventType, out d)) {
                try {
                    d.DynamicInvoke(arg1, arg2);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message);
                }

/*            var callback = d as Callback<T, U>;

            if (callback != null) {
                callback(arg1, arg2);
            }
           else {
                throw CreateBroadcastSignatureException(eventType);
            }*/
            }
        }

        //Three parameters
        public static void Broadcast<T, U, V>(string eventType, T arg1, U arg2, V arg3) {
#if LOG_ALL_MESSAGES || LOG_BROADCAST_MESSAGE
		Debug.Log("MESSENGER\t" + System.DateTime.Now.ToString("hh:mm:ss.fff") + "\t\t\tInvoking \t\"" + eventType + "\"");
#endif
            OnBroadcasting(eventType);

            Delegate d;
            if (EventTable.TryGetValue(eventType, out d)) {
                try {
                    d.DynamicInvoke(arg1, arg2, arg3);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message);
                }

/*            var callback = d as Callback<T, U, V>;

            if (callback != null) {
                callback(arg1, arg2, arg3);
            }
           else {
                throw CreateBroadcastSignatureException(eventType);
            }*/
            }
        }


        //No parameters
        public static void Broadcast(IDispatcher dispatcher, string eventType) {
            Dictionary<string, Delegate> eventTable;

            if (ObserverTable.TryGetValue(dispatcher, out eventTable)) {
                Delegate d;
                if (eventTable.TryGetValue(eventType, out d)) {
                    var callback = d as Callback;

                    if (callback != null) {
                        callback();
                    }
                }
            }
        }

        //Single parameter
        public static void Broadcast<T>(IDispatcher dispatcher, string eventType, T arg1) {
            Dictionary<string, Delegate> eventTable;

            if (ObserverTable.TryGetValue(dispatcher, out eventTable)) {

                Delegate d;
                if (eventTable.TryGetValue(eventType, out d)) {
                    try {
                        d.DynamicInvoke(arg1);
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message);
                    }
                }
            }
        }

        //Two parameters
        public static void Broadcast<T, U>(IDispatcher dispatcher, string eventType, T arg1, U arg2) {
            Dictionary<string, Delegate> eventTable;

            if (ObserverTable.TryGetValue(dispatcher, out eventTable)) {

                Delegate d;
                if (eventTable.TryGetValue(eventType, out d)) {
                    try {
                        d.DynamicInvoke(arg1, arg2);
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message);
                    }
                }
            }
        }

        //Three parameters
        public static void Broadcast<T, U, V>(IDispatcher dispatcher, string eventType, T arg1, U arg2, V arg3) {
            Dictionary<string, Delegate> eventTable;

            if (ObserverTable.TryGetValue(dispatcher, out eventTable)) {

                Delegate d;
                if (eventTable.TryGetValue(eventType, out d)) {
                    try {
                        d.DynamicInvoke(arg1, arg2, arg3);
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message);
                    }
                }
            }
        }

        #endregion

        //Single parameter
        public static Dictionary<string, Delegate> GetEventTable(IDispatcher dispatcher) {
            return ObserverTable[dispatcher];
        }
    }

//This manager will ensure that the messenger's eventTable will be cleaned up upon loading of a new level.
    public sealed class MessengerHelper : MonoBehaviour {
        private void Awake() {
            DontDestroyOnLoad(gameObject);
        }

        //Clean up eventTable every time a new level loads.
        public void OnDisable() {
            Messenger.Cleanup();
        }
    }
}