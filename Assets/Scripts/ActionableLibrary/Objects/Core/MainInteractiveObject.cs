﻿using System.Collections.Generic;
using Assets.ActionableLibrary.Objects.Core;
using Assets.Scripts.Objects.Core;
using UnityEngine;

namespace Assets.Scripts.ActionableLibrary.Objects.Core {
    public abstract class MainInteractiveObject : MonoBehaviour, IDispatcher, Interactive {
        #region Main

        protected HashSet<IDispatcher> ConnectedObjects = new HashSet<IDispatcher>();

        [SerializeField]
        private List<BaseField> _fields;

        private Dictionary<string, BaseField> _fieldsDictionary; 

        private List<BaseField> fields {
            get {
                if (_fields == null) _fields = new List<BaseField>();
                return _fields;
            }
        }

        private Dictionary<string, BaseField> fieldsDictionary {
            get {
                if (_fieldsDictionary == null) {

                    _fieldsDictionary = new Dictionary<string, BaseField>();

                    foreach (var field in fields) {
                        _fieldsDictionary[field.Name] = field;
                    }
                }

                return _fieldsDictionary;
            }
        }

        public BaseField GetField(string key) {
            return fieldsDictionary[key];
        }

        public void SetField(string key, object value) {
            BaseField field;

            if (!fieldsDictionary.TryGetValue(key, out field)) {
                field = new BaseField(key, value);
                fields.Add(field);
                fieldsDictionary[key] = field;
                return;
            }

            field.Value = value;
        }

        public bool HasField(string key) {
            return fieldsDictionary.ContainsKey(key);
        }

        public object GetValue(string key) {
            return GetField(key).Value;
        }

        #region IDispatcher Members

        public void Fire(string eventType) {
            Messenger.Broadcast(this, eventType);
        }

        public void Fire<T>(string eventType, T arg1) {
            Messenger.Broadcast(this, eventType, arg1);
        }

        public void Fire<T, U>(string eventType, T arg1, U arg2) {
            Messenger.Broadcast(this, eventType, arg1, arg2);
        }

        public void Fire<T, U, V>(string eventType, T arg1, U arg2, V arg3) {
            Messenger.Broadcast(this, eventType, arg1, arg2, arg3);
        }

        #endregion

        #region Interactive Members

        public void Connect(IDispatcher from) {
            if (!ConnectedObjects.Contains(from)) {
                ConnectedObjects.Add(from);
                OnConnect(from);
            }
        }

        public void Disconnect(IDispatcher from) {
            if (ConnectedObjects.Contains(from)) {
                ConnectedObjects.Remove(from);
                OnDisconnect(from);
            }
        }

        public void Send(string eventType) {
            Fire(eventType);
        }

        public void Send<T>(string eventType, T arg) {
            Fire(eventType, arg);
        }

        public void Send<T, U>(string eventType, T arg1, U arg2) {
            Fire(eventType, arg1, arg2);
        }

        public void Send<T, U, V>(string eventType, T arg1, U arg2, V arg3) {
            Fire(eventType, arg1, arg2, arg3);
        }

        #endregion

        protected virtual void OnConnect(IDispatcher from) {}
        protected virtual void OnDisconnect(IDispatcher from) {}

        #endregion

        #region IDispatcher Members

        public void AddListener(string eventType, Callback handler) {
            Messenger.AddListener(this, eventType, handler);
        }

        public void AddListener<T>(string eventType, Callback<T> handler) {
            Messenger.AddListener(this, eventType, handler);
        }

        public void AddListener<T, U>(string eventType, Callback<T, U> handler) {
            Messenger.AddListener(this, eventType, handler);
        }

        public void AddListener<T, U, V>(string eventType, Callback<T, U, V> handler) {
            Messenger.AddListener(this, eventType, handler);
        }

        public void RemoveListener(string eventType, Callback handler) {
            Messenger.RemoveListener(this, eventType, handler);
        }

        public void RemoveListener<T>(string eventType, Callback<T> handler) {
            Messenger.RemoveListener(this, eventType, handler);
        }

        public void RemoveListener<T, U>(string eventType, Callback<T, U> handler) {
            Messenger.RemoveListener(this, eventType, handler);
        }

        public void RemoveListener<T, U, V>(string eventType, Callback<T, U, V> handler) {
            Messenger.RemoveListener(this, eventType, handler);
        }

        #endregion
    }
}