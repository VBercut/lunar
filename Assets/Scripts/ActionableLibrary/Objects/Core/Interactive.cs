﻿using Assets.Scripts.Objects.Core;

namespace Assets.ActionableLibrary.Objects.Core {
    public interface Interactive {
	    void Connect(IDispatcher from);

	    void Disconnect(IDispatcher from);

	    void Send(string eventType);

	    void Send<T>(string eventType, T arg);

	    void Send<T, U>(string eventType, T arg1, U arg2);

	    void Send<T, U, V>(string eventType, T arg1, U arg2, V arg3);
	}
}
