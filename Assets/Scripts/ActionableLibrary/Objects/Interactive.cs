﻿using Assets.ActionableLibrary;
using Assets.ActionableLibrary.Objects.Core;
using Assets.Scripts.ActionableLibrary.Objects.Core;
using Assets.Scripts.Objects.Core;
using UnityEngine;

namespace Assets.Scripts.Objects {
    public abstract class Interactive : MainInteractiveObject {
        protected bool InFocus;
        protected bool IsActive = true;
        public bool TurnOn = true;


        // Use this for initialization
        private void Start() {}

        protected override void OnConnect(IDispatcher from) {
            from.AddListener<MainInteractiveObject, string>(PlayerActions.Action, OnAction);
            from.AddListener<MainInteractiveObject>(PlayerActions.GetItemFocus, OnFocus);
            from.AddListener<MainInteractiveObject>(PlayerActions.LostItemFocus, OnLostFocus);
        }


        protected virtual void OnAction(IDispatcher from, string type)
        {
        }

        protected abstract void OnFocus(IDispatcher from);

        protected virtual void OnLostFocus(MainInteractiveObject from) {
            from.RemoveListener<MainInteractiveObject, string>(PlayerActions.Action, OnAction);
            from.RemoveListener<MainInteractiveObject>(PlayerActions.GetItemFocus, OnFocus);
            from.RemoveListener<MainInteractiveObject>(PlayerActions.LostItemFocus, OnLostFocus);
        }
    }
}