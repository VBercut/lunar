﻿using Assets.ActionableLibrary;
using Assets.ActionableLibrary.Objects.Core;
using Assets.Scripts.ActionableLibrary.Objects.Core;
using Assets.Scripts.Objects.Core;
using UnityEngine;

namespace Assets.Scripts.Objects {
    internal abstract class Switcher : Interactive {

        #region Nested type: States

        private enum States
        {
            STAY
        }

        #endregion

        private States CurrentState;
        public AudioSource disableSound;
        public AudioSource switchSound;

        public Color normalColor = Color.white;
        public Color hoverColor = Color.yellow;


        // Use this for initialization
        private void Start() {
            CurrentState = States.STAY;
        }

        protected override void OnConnect(IDispatcher from)
        {
            from.AddListener<MainInteractiveObject, string>(PlayerActions.Action, OnAction);
        }


        protected void OnAction(MainInteractiveObject from, string type) {
/*            if (CurrentState == States.STAY) {
                //if (IsActive) {
                    TurnOn = !TurnOn;
                    switchSound.Play();
                    Fire(ItemEvents.VALUE_CHANGE, TurnOn);
                //}
                //else {
                    disableSound.Play();
                    Fire(ItemEvents.UNACTIVATE);
                //}
            }*/
        }

/*        protected override void OnFocus(MainInteractiveObject from) {
            renderer.material.color = normalColor;
        }*/

        protected override void OnLostFocus(MainInteractiveObject from) {
            renderer.material.color = hoverColor;
        }
    }
}