﻿using Assets.Scripts.Objects.Core;
using UnityEngine;

namespace Assets.Scripts {
    public class RadioTest : MonoBehaviour
    {
        private AudioSource audioTheme;
        public RadioWheel wheel;

        private void Start()
        {
            if (wheel != null)
            {
                Messenger.AddListener(ItemEvents.VALUE_CHANGE, OnChangeVolume);
                Messenger.AddListener(ItemEvents.ACTIVATE, OnTurn);
                Messenger.AddListener(ItemEvents.UNACTIVATE, OnOffTurn);
            }

            audioTheme = GetComponent<AudioSource>();

            audioTheme.Stop();
        }

        private void OnOffTurn()
        {
            audioTheme.Stop();
        }

        private void OnTurn()
        {
            audioTheme.Play();
        }

        private void OnChangeVolume()
        {
            audioTheme.volume = wheel.volume;
        }
    }
}