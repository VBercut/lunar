﻿using Assets.Scripts.Objects.Core;
using UnityEngine;

namespace Assets.Scripts {
    public class SnowFall : MonoBehaviour
    {
        private ParticleSystem snow;

        private void Start()
        {
            snow = GetComponent<ParticleSystem>();
            snow.Stop();

            Messenger.AddListener(ItemEvents.ACTIVATE, OnSnowFall);
            Messenger.AddListener(ItemEvents.UNACTIVATE, OnStopSnowFall);
        }

        private void OnStopSnowFall()
        {
            snow.renderer.enabled = false;
            snow.Clear();
            snow.Stop();
        }

        private void OnSnowFall()
        {
            snow.renderer.enabled = true;
            snow.Play();
        }
    }
}
