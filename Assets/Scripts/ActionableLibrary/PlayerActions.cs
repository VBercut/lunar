﻿namespace Assets.ActionableLibrary {
    public static class PlayerActions {

        public const string Action = "action";

        public const string RollWheel = "roll_wheel";

        public const string GetItemFocus = "inFocus";

        public const string LostItemFocus = "outFocus";

        public const string TakeOut = "takeOut";
        
    }
}

