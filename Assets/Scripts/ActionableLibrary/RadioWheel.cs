﻿using Assets.ActionableLibrary;
using Assets.Scripts.Objects.Core;
using UnityEngine;

namespace Assets.Scripts {
    public class RadioWheel : InteractiveObject
    {
        public bool sourceOn = false;
        public float volume = 1f;

        public void OnAction(string message)
        {
            switch (message)
            {
                case PlayerActions.GetItemFocus:
                    InFocus();
                    break;
                case PlayerActions.RollWheel:
                    OnRollWheel();
                    break;
                case PlayerActions.Action:
                    OnTurn();
                    break;
                case PlayerActions.LostItemFocus:
                    OutFocus();
                    break;
            }
        }

        private void OnRollWheel()
        {
            float scrollValue = Input.GetAxis("Mouse ScrollWheel");
            float oldVolume = volume;

            volume += scrollValue/2;

            volume = Mathf.Min(volume, 1f);
            volume = Mathf.Max(volume, 0);

            if (volume != oldVolume)
            {
                Messenger.Broadcast(ItemEvents.VALUE_CHANGE);
            }
        }

        private void OnTurn()
        {
            sourceOn = !sourceOn;
            Messenger.Broadcast(sourceOn ? ItemEvents.ACTIVATE : ItemEvents.UNACTIVATE);
        }
    }
}