﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


	public static class ItemEvents {
	    public const string IN_FOCUS = "inFocus";
	    public const string OUT_FOCUS = "outFocus";
	    public const string ACTION = "action";
	    public const string ACTIVATE = "activate";
	    public const string UNACTIVATE = "unactivate";
	    public const string VALUE_CHANGE = "valueChange";
	}

