using System;
using Assets.ActionableLibrary;
using Assets.Scripts.Objects.Core;
using UnityEngine;

namespace Assets.Scripts {
    public class InteractiveObject : MonoBehaviour {
        private static String shaderName = "Custom/OutLine";


        private Material borderMaterial;
        private int countBoard;
        private bool focus;
   

        public float outline = 0.05f;

        // Use this for initialization
        protected void Start() {

            Material[] materials = renderer.materials;

            for (int i = 0; i < materials.Length; i++) {
                var mat = (Material) materials.GetValue(i);
                if (mat.shader.name == shaderName) {
                    borderMaterial = mat;
                    break;
                }
            }
        }

        // Update is called once per frame
        private void Update() {
        }

        // ������� � ������
        public virtual void OnAction(string message) {
            switch (message) {
                case PlayerActions.GetItemFocus:
                    InFocus();
                    break;
                case PlayerActions.Action:
                    Action();
                    break;
                case PlayerActions.LostItemFocus:
                    OutFocus();
                    break;
            }
        }

        protected void InFocus()
        {
            borderMaterial.SetFloat("_Outline", outline); // �������� �������� � �������
            Debug.Log("You can see me!");
            focus = true;
        }

        protected void OutFocus() {
            borderMaterial.SetFloat("_Outline", 0); // ������ ������� �� �����, ��� ��� ������� �� � ������
            Messenger.RemoveListener<string>("action", OnAction);
            Debug.Log("Haha, You can't take me!");
            focus = false;
        }

        public bool IsFocus() {
            return focus;
        }


        protected void Action() {
            Debug.Log("Oh, no! You shouldn't take me!");
            countBoard++;
            Debug.Log(countBoard);
        }
    }
}